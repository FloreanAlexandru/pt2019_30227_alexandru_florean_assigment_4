package BusinessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BaseProduct(int id, String nume, double pret) {
		super();
		this.id=id;
		this.nume=nume;
		this.pret=pret;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public double getPret() {
		return pret;
	}

	public void computePrice(double pret) {
		this.pret = pret;
	}
	
	
	public String toString() {
		return "\n Base Product \n id: " + id + "\n nume:" + nume + "\n pret:" + pret;
	}
	
}
