package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class AdministratorGraphicalUserInterface {

	private JButton buton1 = new JButton("Base Product");
	private JButton buton2 = new JButton("Composite Product");
	private JButton buton3 = new JButton("Delete Composite Product");
	private JButton buton5 = new JButton("Update Composite Product");
	private JButton buton6 = new JButton("Delete Base Product");
	private JButton buton4 = new JButton("Update Base Product");
	private JButton buton7 = new JButton("Show B.Products");
	private JButton buton8 = new JButton("Show C.Products");
	
	private JLabel id1 = new JLabel("Id Base Product");
	private JLabel nume = new JLabel("Nume Produs");
	private JLabel pret = new JLabel("Pret produs");
	private JLabel id2 =new JLabel("Id Composite Product");
	
	private JTextField idb= new JTextField(25);
	private JTextField numep= new JTextField(25);
	private JTextField pretp= new JTextField(25);
	private JTextField idc= new JTextField(25);
	
	public Restaurant r;
	
	public AdministratorGraphicalUserInterface(final Restaurant r) {
		this.r=r;
		
		JFrame administrator = new JFrame("Administrator");
		administrator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		administrator.setSize(500,500);
	
		JPanel c1 = new JPanel();
		JPanel c2 = new JPanel();
		JPanel c3 = new JPanel();
		JPanel c4 = new JPanel();
		JPanel c5 = new JPanel();
		JPanel c6 = new JPanel();
		JPanel c7 = new JPanel();
		JPanel c8 = new JPanel();
		JPanel c9 = new JPanel();
		
		c1.add(buton1);
		c1.add(buton2);
		
		c7.add(buton3);
		c7.add(buton5);
		
		c8.add(buton6);
		c8.add(buton4);
		
		c9.add(buton7);
		c9.add(buton8);
		
		c1.setLayout(new FlowLayout());
		c7.setLayout(new FlowLayout());
		c8.setLayout(new FlowLayout());
		c9.setLayout(new FlowLayout());
		
		c2.add(id1);	c2.add(idb);	c2.setLayout(new FlowLayout());
		c3.add(nume);	c3.add(numep); 	c3.setLayout(new FlowLayout());
		c4.add(pret);	c4.add(pretp);	c4.setLayout(new FlowLayout());
		c5.add(id2);	c5.add(idc);	c5.setLayout(new FlowLayout());
		
		c6.add(c2);	c6.add(c3);	c6.add(c4);	c6.add(c5);	c6.add(c1); c6.add(c7); c6.add(c8);
		c6.add(c9);
		c6.setLayout(new BoxLayout(c6,BoxLayout.Y_AXIS));
		
		administrator.add(c6);
		administrator.setVisible(true);
		
		//
		buton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//r.showProduct();
				ArrayList<MenuItem> x = r.showProduct();
				
				String[] columns= {"idProduct","Nume","Pret"};
				 String data[][]= new String[50][3];
				 String row;
				 int indexMatrice=0;
				 for(MenuItem a: x) {
					 if(a instanceof BaseProduct) {
						 String[] rowElements = new String[3];
						 rowElements[0]=Integer.toString(a.getId());
						 rowElements[1]=new String(a.getNume());
						 rowElements[2]=Double.toString(a.getPret());
						 data[indexMatrice]=rowElements;
						 indexMatrice++;
					 	}
					 }
				
				 JTable table1 = new JTable(data,columns);
				 table1.setCellSelectionEnabled(true);
				 table1.setBounds(300,400,200,300);
				 JScrollPane sp = new JScrollPane(table1);
				 
				 JFrame f=new JFrame();
				 f.add(sp);
				 f.setSize(300,400);
				 f.setVisible(true);
			}
		});
		
		buton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//r.showProduct();
				ArrayList<MenuItem> x = r.showProduct();
				
				String[] columns= {"idProduct","Nume","Pret"};
				 String data[][]= new String[50][3];
				 String row;
				 int indexMatrice=0;
				 for(MenuItem a: x) {
					 if(a instanceof CompositeProduct) {
						 String[] rowElements = new String[3];
						 rowElements[0]=Integer.toString(a.getId());
						 rowElements[1]=new String(a.getNume());
						 rowElements[2]=Double.toString(a.getPret());
						 data[indexMatrice]=rowElements;
						 indexMatrice++;
					 	}
					 }
				
				 JTable table1 = new JTable(data,columns);
				 table1.setCellSelectionEnabled(true);
				 table1.setBounds(300,400,200,300);
				 JScrollPane sp = new JScrollPane(table1);
				 
				 JFrame f=new JFrame();
				 f.add(sp);
				 f.setSize(300,400);
				 f.setVisible(true);
			}
		});
		
		//insert base product
		buton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idBP=idb.getText() ;
				String Nume=numep.getText();
				String Pret=pretp.getText();

				int idbp = Integer.parseInt(idBP);
				double pret = Double.parseDouble(Pret);
				
				MenuItem x= new BaseProduct(idbp,Nume,pret);
				r.insertBaseProduct(x, 0);
				r.sortare();
				
			}
		});
		
		//insert composite product
		buton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idBP=idb.getText() ;
				String Nume=numep.getText();
				String Pret=pretp.getText();
				String idCP=idc.getText();

				String[] s= idBP.split(",");
				
				int idcp = Integer.parseInt(idCP);
				ArrayList<MenuItem> lista= r.getMeniu();
				double pret=0;
				
				for(String b : s) {
					int id= Integer.parseInt(b);
						for(MenuItem a: lista) {
							if(a.getId()==id && a instanceof BaseProduct) {
							pret= a.getPret()+pret;
							}
						}
				}
				
				MenuItem x= new CompositeProduct(idcp,Nume,pret);
				r.insertCompositeProduct(x,0);
				r.sortare1();
			}
		});
		
		//Update BP
		buton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idCP=idb.getText() ;
				String Nume=numep.getText();
				String Pret=pretp.getText();

				int idcp = Integer.parseInt(idCP);
				double pret = Double.parseDouble(Pret);
				
				r.updateBProduct(idcp, pret, Nume);
				
			}
		});
		
		//delete CP
		buton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idCP=idc.getText() ;

				int idcp = Integer.parseInt(idCP);
				
				r.deleteCProduct(idcp);
				r.refacere1();
			}
		});
		
		//update CP
		buton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idCP=idc.getText() ;
				String Nume=numep.getText();
				String Pret=pretp.getText();

				int idcp = Integer.parseInt(idCP);
				double pret = Double.parseDouble(Pret);
				
				r.updateCProduct(idcp, pret, Nume);
			}
		});
		
		//delete BP
		buton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String idBP=idb.getText() ;
				int idbp = Integer.parseInt(idBP);
				
				r.deleteBProduct(idbp);
				r.refacere();
			}
		});
		
		
	}
	
	public String getId() {
		return idb.getText();
	}
	
	public String getNume() {
		return numep.getText();
	}
	
	public String getPret() {
		return pretp.getText();
	}
	
	public String getIdC() {
		return idc.getText();
	}
	
	public void butonBaseProduct(ActionListener a) {
		buton1.addActionListener(a);
	}
	
	public void butonCompositeProduct(ActionListener a) {
		buton2.addActionListener(a);
	}
	
	public void butonDeleteC(ActionListener a) {
		buton3.addActionListener(a);
	}
	
	public void butonUpdateB(ActionListener a) {
		buton4.addActionListener(a);
	}
	
	public void butonUpdateC(ActionListener a) {
		buton5.addActionListener(a);
	}
	
	public void butonDeleteB(ActionListener a) {
		buton6.addActionListener(a);
	}
	
	public void butonShow(ActionListener a) {
		buton7.addActionListener(a);
	}
	
}
