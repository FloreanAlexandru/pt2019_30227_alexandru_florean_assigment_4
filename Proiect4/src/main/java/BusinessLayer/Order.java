package BusinessLayer;

import java.sql.Date;
import java.util.zip.*;
public class Order {
	
	int idComanda;
	Date data;
	int masa;
	
	public Order(int idComanda, Date data, int masa) {
		super();
		this.idComanda = idComanda;
		this.data = data;
		this.masa = masa;
	}
	
	public int getIdComanda() {
		return idComanda;
	}
	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public int getMasa() {
		return masa;
	}
	public void setMasa(int masa) {
		this.masa = masa;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + idComanda;
		result = prime * result + masa;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (idComanda != other.idComanda)
			return false;
		if (masa != other.masa)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\nOrder:\n idComanda" + idComanda + " \ndata" + data + "\n masa" + masa +"\n";
	}
	
	
}
