package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.FileWriter;
import java.util.*;

public class WaiterGraphicalUserInterface {

	private JButton buton1 = new JButton("Adauga Comanda");
	private JButton buton2 = new JButton("Pret Comanda");
	private JButton buton3 = new JButton("Factura");
	private JButton buton4 = new JButton("Afisare Comenzi");

	private JLabel idO= new JLabel("Id Order");
	private JLabel idB= new JLabel("Id Base Products");
	private JLabel idC= new JLabel("Id Composite Products");
	//private JLabel date= new JLabel("Date");
	private JLabel masa= new JLabel("Table");
	
	private JTextField ido = new JTextField(30);
	private JTextField idb = new JTextField(30);
	private JTextField idc = new JTextField(30);
	private JTextField data = new JTextField(30);
	private JTextField table = new JTextField(30);
	
	public Restaurant r;
	
	public WaiterGraphicalUserInterface(final Restaurant r) {
		
		this.r=r;
		
		final JFrame Waiter = new JFrame("Waiter");
		Waiter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Waiter.setSize(500,300);
		
		JPanel c1= new JPanel();
		JPanel c2= new JPanel();
		JPanel c3= new JPanel();
		//JPanel c4= new JPanel();
		JPanel c5= new JPanel();
		JPanel c6= new JPanel();
		JPanel c7= new JPanel();
		
		c1.add(idO);
		c1.add(ido);
		c1.setLayout(new FlowLayout());
		
		c2.add(idB);
		c2.add(idb);
		c2.setLayout(new FlowLayout());
		
		c3.add(idC);
		c3.add(idc);
		c3.setLayout(new FlowLayout());
		
		//c4.add(date);
		//c4.add(data);
		//c4.setLayout(new FlowLayout());
		
		c5.add(masa);
		c5.add(table);
		c5.setLayout(new FlowLayout());
		
		c6.add(buton1);
		c6.add(buton2);
		c6.add(buton3);
		c6.add(buton4);
		c6.setLayout(new FlowLayout());
		
		c7.add(c1);
		c7.add(c2);
		c7.add(c3);
		//c7.add(c4);
		c7.add(c5);
		c7.add(c6);
		c7.setLayout(new BoxLayout(c7,BoxLayout.Y_AXIS));
		
		Waiter.add(c7);
		Waiter.setVisible(true);
		
		buton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ArrayList<MenuItem> nou = new ArrayList<MenuItem>();
				
				String idBP= idb.getText();
				String idCP= idc.getText();
				String idO= ido.getText();
				String masuta= table.getText();

				int ido=Integer.parseInt(idO);
				int masa= Integer.parseInt(masuta);
				Date data= new Date(119 , 4 , 23);
				Order bon = new Order(ido,data,masa);
				
				String[] base= idBP.split(",");
				String[] compose= idCP.split(",");
				ArrayList<MenuItem> lista = r.getMeniu();
				
				for(String a : base) {
					int id= Integer.parseInt(a);
					for(MenuItem x: lista) {
						if(x.getId()==id && x instanceof BaseProduct) {
							nou.add(x);
						}
					}
				}
				
				for(String a : compose) {
					int id= Integer.parseInt(a);
					for(MenuItem x: lista) {
						if(x.getId()==id && x instanceof CompositeProduct) {
							nou.add(x);
						}
					}
				}
				
				r.adaugaComanda(bon, nou);
			}
		});
		
		buton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<MenuItem> x =null;
				
				String idO= ido.getText();
				int id=Integer.parseInt(idO);
				x=r.returnOrder(id);
				double pret=0;
				
				if(pret!=0) {
					pret=0;
				}
				
				for(MenuItem a: x) {
					pret=a.getPret()+pret;
				}
				String spret =String.valueOf(pret) ;
				
				JPanel c1=new JPanel();
				JOptionPane.showMessageDialog(Waiter,"Pret pe comanda "+ spret);
			}
		});
		
		buton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String idO= ido.getText();
				int id=Integer.parseInt(idO);
				
				Order x = r.returnOrder1(id);
				ArrayList<MenuItem> y = r.returnOrder(id);
				
				System.out.println(x+"AICI");
				FileWriter.AfisareFacturi(x,y);
			}
			
		});
		
		
		buton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				ArrayList<Order> x = r.afisareComenzi();
				
				String[] columns= {"idComanda","Data","Masa"};
				 String data[][]= new String[50][3];
				 String row;
				 int indexMatrice=0;
				 for(Order a: x) {
					 	String[] rowElements = new String[3];
					 	rowElements[0]=Integer.toString(a.getIdComanda());
					 	rowElements[1]=a.getData().toString();
					 	rowElements[2]=Integer.toString(a.getMasa());
					 	data[indexMatrice]=rowElements;
					 	indexMatrice++;
					 }
				
				 JTable table1 = new JTable(data,columns);
				 table1.setCellSelectionEnabled(true);
				 table1.setBounds(300,400,200,300);
				 JScrollPane sp = new JScrollPane(table1);
				 
				 JFrame f=new JFrame();
				 f.add(sp);
				 f.setSize(300,400);
				 f.setVisible(true);
			}
		});
	}
}
