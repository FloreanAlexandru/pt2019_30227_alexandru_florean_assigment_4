package PresentationLayer;

import javax.swing.JButton;
import javax.swing.JFrame;

import BusinessLayer.Restaurant;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MainController extends JFrame {

	private JButton AdministratorButon = new JButton("Administrator");
	private JButton ChefButon = new JButton("Chef");
	private JButton OspatarButon = new JButton("Ospatar");
	
	public Restaurant r;
	
	public MainController(final Restaurant r) {
		
	this.r=r;
		
	JFrame Main = new JFrame("Main");
	
	Main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Main.setSize(300,100);
	
	JPanel c1= new JPanel();
	
	c1.add(AdministratorButon);
	c1.add(ChefButon);
	c1.add(OspatarButon);
	c1.setLayout(new BoxLayout(c1,BoxLayout.X_AXIS));
	c1.setVisible(true);
	
	JPanel c2 = new JPanel();
	c2.add(c1);
	c2.setLayout(new FlowLayout());
	
	Main.add(c2);
	Main.setVisible(true);

	}
	
	public void butonAdministrator(ActionListener a) {
		AdministratorButon.addActionListener(a);
	}
	
	public void butonChef(ActionListener a) {
		ChefButon.addActionListener(a);
	}
	
	public void butonOspatar(ActionListener a) {
		OspatarButon.addActionListener(a);
	}
	
}
