package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String nume;
	public double pret;
	ArrayList<MenuItem> ingrediente = new ArrayList<MenuItem>();
	
	public CompositeProduct(int id,String nume,double pret) {
		super();
		this.id=id;
		this.nume=nume;
		this.pret=pret;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public double getPret() {
		return pret;
	}

	public void setPret(double pret) {
		this.pret = pret;
	}

	public ArrayList<MenuItem> getMeniu() {
		return ingrediente;
	}

	public void setMeniu(ArrayList<MenuItem> ingrediente) {
		this.ingrediente = ingrediente;
	}

	public String toString() {
		return "\nCompositeProduct \nid:" + id + "\n nume:" + nume + "\n pret=" + pret + "\n meniu" + ingrediente+"\n";
	}

	public void ComputePrice() {
		double pretTotal=0;
		
		for(MenuItem m: ingrediente) {
			pretTotal=pretTotal + m.getPret();
		}
		
		pret= pretTotal;
	}
	
}