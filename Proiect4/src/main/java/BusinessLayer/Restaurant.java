package BusinessLayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import javax.swing.JLabel;

import DataLayer.RestaurantSerializator;
public class Restaurant extends Observable {
	
	ArrayList<MenuItem> meniu = new ArrayList<MenuItem>();
	HashMap<Order, ArrayList<MenuItem> > comenzi = new HashMap <Order, ArrayList<MenuItem>>();
	
	public Restaurant() {
		super();
		//RestaurantSerializator.Serializare(meniu);
		meniu=RestaurantSerializator.Deserializare();
		 
	}
	
	public ArrayList<MenuItem> getMeniu() {
		return meniu;
	}

	public void setMeniu(ArrayList<MenuItem> meniu) {
		this.meniu = meniu;
	}

	public void refacere(){
		//ArrayList<MenuItem> meniu1 = meniu;
		//ArrayList<MenuItem> meniu2 = new ArrayList<MenuItem>();
		int j=0;
		for(int i=0;i<meniu.size();i++) {
		if(meniu.get(i) instanceof BaseProduct) {
			meniu.get(i).setId(j);
			j++;}
		}
	
		RestaurantSerializator.Serializare(meniu);
	}
	
	public void refacere1(){
		//ArrayList<MenuItem> meniu1 = meniu;
		//ArrayList<MenuItem> meniu2 = new ArrayList<MenuItem>();
		int j=0;
		for(int i=0;i<meniu.size();i++) {
		if(meniu.get(i) instanceof CompositeProduct) {
			meniu.get(i).setId(j);
			j++;}
		}
	
		RestaurantSerializator.Serializare(meniu);
	}
	
	public void sortare() {
		MenuItem x=null;
		
		for(int i=0;i<meniu.size();i++) {
			for(int j=i+1;j<meniu.size();j++) {
				if(meniu.get(i).getId()>meniu.get(j).getId() &&
						(meniu.get(i) instanceof BaseProduct) && 
						(meniu.get(j) instanceof BaseProduct) ) {
					x=new BaseProduct(meniu.get(i).getId(),meniu.get(i).getNume(),meniu.get(i).getPret());
					
					meniu.get(i).setId(meniu.get(j).getId());
					meniu.get(i).setNume(meniu.get(j).getNume());
					meniu.get(i).setPret(meniu.get(j).getPret());
					
					meniu.get(j).setId(x.getId());
					meniu.get(j).setNume(x.getNume());
					meniu.get(j).setPret(x.getPret());
					
			}
		}
	}
		RestaurantSerializator.Serializare(meniu);
}
	
	public void sortare1() {
		MenuItem x=null;
		
		for(int i=0;i<meniu.size();i++) {
			for(int j=i+1;j<meniu.size();j++) {
				if(meniu.get(i).getId()>meniu.get(j).getId() &&
						(meniu.get(i) instanceof CompositeProduct) && 
						(meniu.get(j) instanceof CompositeProduct) ) {
					x=new CompositeProduct(meniu.get(i).getId(),meniu.get(i).getNume(),meniu.get(i).getPret());
					
					meniu.get(i).setId(meniu.get(j).getId());
					meniu.get(i).setNume(meniu.get(j).getNume());
					meniu.get(i).setPret(meniu.get(j).getPret());
					
					meniu.get(j).setId(x.getId());
					meniu.get(j).setNume(x.getNume());
					meniu.get(j).setPret(x.getPret());
			}
		}
	}
		RestaurantSerializator.Serializare(meniu);
}
	
	public void insertBaseProduct(MenuItem x,int idc) {
		
		int max=0;
		int max1=0;
		
		for(MenuItem a : meniu) {
			if(a.getId()>max && (a instanceof BaseProduct)) {
				max1=max;
				max=a.getId();
			}
		}
		
		for(MenuItem a: meniu) {
		if(a.getId()==x.getId() && (a instanceof BaseProduct) && (max-max1==1)) {
			x.setId(max+1);
		}else if ((max-max1!=1) && a.getId()==x.getId() && (a instanceof BaseProduct) ) {
			x.setId(max1+1);
			}
		}
		meniu.add(x);
		RestaurantSerializator.Serializare(meniu);
	
}

	public void insertCompositeProduct(MenuItem x,int idc) {
		
		int max=0;
		int max1=0;
		
		for(MenuItem a : meniu) {
			if(a.getId()>max && (a instanceof CompositeProduct)) {
				max1=max;
				max=a.getId();
			}
		}
		
		for(MenuItem a: meniu) {
		if(a.getId()==x.getId() && (a instanceof CompositeProduct) && (max-max1==1)) {
				x.setId(max+1);
			}else if(max-max1!=0 && a instanceof CompositeProduct && a.getId()==x.getId()) {
				x.setId(max1+1);
			}
		}
		meniu.add(x);
		RestaurantSerializator.Serializare(meniu);
	}
	
	public void deleteBProduct(int id) {
		MenuItem x = null; //new BaseProduct(0,"",0);
		int i=0;
		for(MenuItem a : meniu) {
			i++;
			if(a.getId()==id && (a instanceof BaseProduct)) {
				//a.setId(x.getId());
				//a.setNume(x.getNume());
				//a.setPret(x.getPret());
				//meniu.remove(a);
				//meniu.remove(i);
				x=a;
				break;
			}
		}
		meniu.remove(x);
		
		RestaurantSerializator.Serializare(meniu);
		
	}
	
	public void deleteCProduct(int id) {
		MenuItem x = null;//new CompositeProduct(0,"",0);
		int i=0;
		for(MenuItem a : meniu) {
			i++;
			
			if(a.getId()==id && (a instanceof CompositeProduct)) {
				//a.setId(x.getId());
				//a.setNume(x.getNume());
				//a.setPret(x.getPret());
				x=a;
				break;
				//meniu.remove(a);
				//meniu.remove(i);
			}
		}
		meniu.remove(x);
		RestaurantSerializator.Serializare(meniu);
		
	}
	
	public void updateBProduct(int id,double pret,String nume) {
		
		for(MenuItem a: meniu) {
			if(a.getId()==id && (a instanceof BaseProduct)) {
				a.setPret(pret);
				a.setNume(nume);
			}
		}
		RestaurantSerializator.Serializare(meniu);
	}
	
	public void updateCProduct(int id,double pret,String nume) {
		
		for(MenuItem a: meniu) {
			if(a.getId()==id && (a instanceof CompositeProduct)) {
				a.setPret(pret);
				a.setNume(nume);
			}
		}
		RestaurantSerializator.Serializare(meniu);
	}
	
	public ArrayList<MenuItem> showProduct() {
		ArrayList<MenuItem> meniul=new ArrayList<MenuItem>();
		
		for(MenuItem a: meniu) {
			System.out.println(a.getId()+" "+a.getNume()+" "+a.getPret()+" ");
			meniul.add(a);
		}
		
		return meniul;
	}
	
	public MenuItem getBase(int a) {
		MenuItem nou=null;
		for(MenuItem x: meniu) {
			if(x.getId()==a && x instanceof BaseProduct) {
				nou=x;
				break;
			}
		}
		return nou;
	}
	
	public MenuItem getCompute(int a) {
		MenuItem nou=null;
		for(MenuItem x: meniu) {
			if(x.getId()==a && x instanceof CompositeProduct) {
				nou=x;
				break;
			}
		}
		return nou;
		
	}
	
	public void adaugaComanda(Order a,ArrayList<MenuItem> b) {
		comenzi.put(a, b);
		setChanged();
		notifyObservers(comenzi);
		//RestaurantSerializator.Serializare(comenzi);

	}
	
	public ArrayList<Order> afisareComenzi() {
		ArrayList<Order> a = new ArrayList<Order>();
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : comenzi.entrySet()) {
			Order key= entry.getKey();
			a.add(key);
		}
		
		return a;
}
	public ArrayList<MenuItem> returnOrder(int id) {
		ArrayList<MenuItem> x=null; //new ArrayList<MenuItem>();
		
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : comenzi.entrySet()) {
			Order key = entry.getKey();
				if(key.getIdComanda()==id) {
					x= entry.getValue();
					break;
			}
		}
		
		return x;
	}
	
	public Order returnOrder1(int id) {
		Order x=null;
		
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : comenzi.entrySet()) {
			Order key = entry.getKey();
				if(key.getIdComanda()==id) {
					x= key;
					break;
			}
		}
		
		return x;
	}
	
}
