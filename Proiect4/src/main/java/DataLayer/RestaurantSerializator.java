package DataLayer;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import BusinessLayer.MenuItem;
public class RestaurantSerializator implements java.io.Serializable {
	
	public int id;
	public double pret;
	public String nume;
	public ArrayList<MenuItem> comenzi = new ArrayList<MenuItem>();
	
	public RestaurantSerializator(int id,String nume,double pret) {
		this.id=id;
		this.pret=pret;
		this.nume=nume;
	}
	
	public static void Serializare(ArrayList<MenuItem> comenzi) {
		
		 try
	        {    
	            String filename = "Restaurant.ser";
	            FileOutputStream file = new FileOutputStream(filename); 
	            ObjectOutputStream out = new ObjectOutputStream(file);        
	            out.writeInt(comenzi.size());
	            
	            if(comenzi.size()!=0) {
	            	for(MenuItem a : comenzi) {
	            		out.writeObject(a);
	            	}
	            }
	            out.close(); 
	            file.close();
	        } 
	        catch(IOException ex) 
	        { 
	            System.out.println("IOException is caught1"); 
	            ex.printStackTrace();
	        } 
	}
	
	public static ArrayList<MenuItem> Deserializare() {
		MenuItem x =null;
		ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
		int length=0;
		
		try
        {    
			String filename = "Restaurant.ser";
            FileInputStream file = new FileInputStream(filename); 
            ObjectInputStream in = new ObjectInputStream(file); 
            
            try {
            	length = in.readInt();
            }
            catch(IOException error){
            	length = 0;
            }
            
            int i=0;
            while(i<length) {
            	
            	x=(MenuItem)in.readObject();
            	menu.add(x);
            	i++;
            }
            
            in.close(); 
            file.close(); 
            
           /* for(MenuItem a: menu) {
            	System.out.println(a.getId()+""+a.getNume()+"   aaaa");
            }*/
            
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
            ex.printStackTrace();
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException is caught"); 
            ex.printStackTrace();
        }
    return menu;
	} 
}
