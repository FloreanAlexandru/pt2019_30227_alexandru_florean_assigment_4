package PresentationLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class ChefGraphicalUserInterface implements Observer {

	public Restaurant r;
	JTextArea text;
	
	public ChefGraphicalUserInterface(final Restaurant r) {
		
		this.r=r;
		
		JFrame Chef = new JFrame("Chef");
		text = new JTextArea(25,50);
		
		Chef.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Chef.setSize(600,450);
		
		JPanel c1 =new JPanel();
		JScrollPane jsp = new JScrollPane(text);
		jsp.setHorizontalScrollBarPolicy(jsp.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		c1.add(jsp);
		Chef.add(c1);
		
		Chef.setVisible(true);
		
	}

	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		HashMap<Order, ArrayList<MenuItem>> mapa = (HashMap<Order, ArrayList<MenuItem>>) arg;
		//System.out.println(mapa.toString());
		text.setText(mapa.toString());
		
		
	}
}
