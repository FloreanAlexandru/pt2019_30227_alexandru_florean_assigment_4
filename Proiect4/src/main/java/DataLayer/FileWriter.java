package DataLayer;

import BusinessLayer.Order;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import BusinessLayer.MenuItem;
public class FileWriter {

	public static void AfisareFacturi(Order a,ArrayList<MenuItem> b) {
		
		try {
			PrintWriter bon = new PrintWriter("Nota de plata.txt","UTF-8");
			bon.println("Comanda: "+a.getIdComanda());
			bon.println("Masa: "+a.getMasa());
			bon.println("Data: "+a.getData());
			
			for(MenuItem c : b) {
				bon.println(c.getNume()+"->"+c.getId());
			}
			bon.close();
		}catch(Exception ex) {
			System.out.println(ex.getMessage()+"la scriere in fisier");}
	}
}
