package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class Start {
	public Restaurant restaurant;

	public Start(Restaurant r) {
		MainController start = new MainController(restaurant);
		
		restaurant = r;
		
		start.butonChef(new ChefListener());
		start.butonAdministrator(new AdministratorListener());
		start.butonOspatar(new WaiterListener());
	}
	

	class ChefListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			ChefGraphicalUserInterface chef = new ChefGraphicalUserInterface(restaurant);
			restaurant.addObserver(chef);
		}
	}
	
	class WaiterListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			WaiterGraphicalUserInterface chef = new WaiterGraphicalUserInterface(restaurant);
		}
	}
	class AdministratorListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			AdministratorGraphicalUserInterface chef = new AdministratorGraphicalUserInterface(restaurant);
		}
	}
	
	public static void main (String[] args) {
		
		Restaurant r= new Restaurant();
		Start strat= new Start(r);
		
		//ArrayList<MenuItem> comenzi = new ArrayList<MenuItem>();
		
		//MenuItem a= new BaseProduct(1,"Margarina",5);
		//MenuItem b= new BaseProduct(2,"Margarina",5);
		//MenuItem c= new CompositeProduct(1,"Paine cu margarina",6);
		//Restaurant r= new Restaurant();
		
		//r.insertBaseProduct(a, 0);
		//r.insertBaseProduct(b, 0);
		//r.insertCompositeProduct(c, 0);
		
		//r.deleteBProduct(7);
		//r.deleteBProduct(5);
		//r.deleteCProduct(3);
		
		//r.updateBProduct(3,9,"Salata");
		//r.insertCompositeProduct(c, 0);
		
		//r.showProduct();
		
		
	}
}
